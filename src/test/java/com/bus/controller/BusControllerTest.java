package com.bus.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bus.dto.BusDto;
import com.bus.model.Bus;
import com.bus.service.BusService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BusControllerTest {

	@InjectMocks
	BusController busController;

	@Mock
	BusService busService;

	static Bus bus = null;

	@BeforeClass
	public static void setUp() {
		bus = new Bus();

	}

	@Test
	public void testsearchbusForPositive() {
		List<Bus> buses = new ArrayList();
		Bus bus = new Bus();
		BusDto busDto =new BusDto();
		bus.setBusId(1);
		//bus.setBusname("redrose");
		bus.setSource("salem");
		bus.setDestination("behar");
		Mockito.when(busService.searchbus(busDto)).thenReturn(buses);
		ResponseEntity<List<Bus>> buess = busController.searchbus(busDto);
		Assert.assertNotNull(buess);
        Assert.assertEquals(buess.getStatusCode(), HttpStatus.OK);
        
		/*Mockito.when(busController.searchbus(bus)).thenReturn((ResponseEntity<List<Bus>>) buses);
		List<Bus> buess = busService.searchbus(null);
		Assert.assertNotNull(buess);
		Assert.assertEquals(buess.size(), HttpStatus.OK);*/
	}
	@Test
	public void testsearchbusForNagative() {
		List<Bus> buses = new ArrayList();
		Bus bus = new Bus();
		bus.setBusId(-1);
		BusDto busDto =new BusDto();
	//	bus.setBusname("redrose");
		bus.setSource("salem");
		bus.setDestination("behar");
		Mockito.when(busService.searchbus(busDto)).thenReturn(buses);
		ResponseEntity<List<Bus>> buess = busController.searchbus(busDto);
		Assert.assertNotNull(buess);
        Assert.assertEquals(buess.getStatusCode(), HttpStatus.OK);
	}

	@AfterClass
	public static void tearDown() {
		bus = null;
	}

}
