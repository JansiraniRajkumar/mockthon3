package com.bus.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.bus.dto.BookingDto;
import com.bus.model.Booking;
import com.bus.repository.BusRepo;
import com.bus.service.BusBookingService;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingControllerTest {
	
	
	@InjectMocks
	BusBookingController busBookingController;
	
	@Mock
	BusBookingService busBookingService;
	@Test
	public void testBusbooking() {
		BookingDto bookingDto =new BookingDto();
		Booking booking =new Booking();
		bookingDto.setBusId(1);
		bookingDto.setEmailId("sai@gmail.com");
		Mockito.when(busBookingService.makeBooking(bookingDto)).thenReturn(booking);
		Assert.assertEquals(booking, busBookingService.makeBooking(bookingDto));
		
	}
	
	@Test
	public void testBusbookingpos() {
		BookingDto bookingDto =new BookingDto();
		Booking booking =new Booking();
		bookingDto.setBusId(1);
		bookingDto.setEmailId("sai@gmail.com");
		Mockito.when(busBookingService.makeBooking(bookingDto)).thenReturn(booking);
		Assert.assertNotNull(booking);
		ResponseEntity<Booking> b=busBookingController.makeBooking(bookingDto);
	
		
		Assert.assertEquals(HttpStatus.OK, b.getStatusCode());
		
	}
	
}
