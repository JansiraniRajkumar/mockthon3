package com.bus.service;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.bus.dto.BookingDto;
import com.bus.exception.DataNotFound;
import com.bus.exception.UserNotfoundException;
import com.bus.model.Booking;
import com.bus.model.Bus;
import com.bus.model.User;
import com.bus.repository.BusBookingRepo;
import com.bus.repository.BusRepo;
import com.bus.repository.UserRepo;

import junit.framework.Assert;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BookingServiceTest {
	@InjectMocks
	BusBookingServiceImpl busBookingServiceImpl;
	@Mock
	BusBookingRepo busBookingRepo;
	
	@Mock
	BusRepo busRepo;
	@Mock
	UserRepo userRepo;
	@Test(expected = UserNotfoundException.class)
	public void bookBusServicefrusr() {
		BookingDto bookingDto = new BookingDto();
		Booking booking =new Booking();
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(null);
		busBookingServiceImpl.makeBooking(bookingDto);
	}
	
	@Test(expected = DataNotFound.class)
	public void bookBusServiceTestbus() {
		BookingDto bookingDto = new BookingDto();
		Booking booking =new Booking();
		User usr =new User();
		usr.setEmail("sk");
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
		Assert.assertNotNull(usr);
		Assert.assertEquals(booking, busBookingServiceImpl.makeBooking(bookingDto));
		
	}
	
	@Test(expected = NullPointerException.class)
	public void bookBusServicef() {
		BookingDto bookingDto = new BookingDto();
		
		bookingDto.setNoOfSeatsBooked(2);
		bookingDto.setBusId(1);
		Booking booking =new Booking();
		User usr =new User();
		BeanUtils.copyProperties(bookingDto, booking);
		Bus  b= new Bus();
		b.setFare(100.0);
		Optional<Bus> bus = Optional.ofNullable(new Bus());
		//bus.setBusId(1);
		
		usr.setEmail("sk");
		Mockito.when(userRepo.findByEmail(bookingDto.getEmailId())).thenReturn(usr);
		Mockito.when(busRepo.findById(1)).thenReturn(bus);
		busRepo.save(b);
		booking.setAmount(b.getFare()*bookingDto.getNoOfSeatsBooked());
		booking.setUser(usr);
		booking.setBus(b);
		
		Assert.assertNotNull(usr);
		Assert.assertEquals(booking, busBookingServiceImpl.makeBooking(bookingDto));
		
	}
	
	
	
	
	
	
	
	

}
