package com.bus.exception;

import java.sql.Time;

public class BusNotFoundException extends RuntimeException{
	
	public BusNotFoundException(String source) {
		super(String.format("bus with %s not found",source));
	}

}