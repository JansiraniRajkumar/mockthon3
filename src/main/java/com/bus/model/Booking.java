package com.bus.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "booking")
public class Booking {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int bookingId;
	@Temporal(TemporalType.DATE)
	private Date bookingDate;
	private int noOfSeatsBooked;
	private Double amount;
	@Temporal(TemporalType.DATE)
	private Date journeyDate;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JoinColumn(name="busId")
	private Bus bus;
	
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
	@JsonIgnore
	@JoinColumn(name = "userId")
	private User user;
	
	public Date getJourneyDate() {
		return journeyDate;
	}

	public void setJourneyDate(Date journeyDate) {
		this.journeyDate = journeyDate;
	}

	public Booking() {
		super();
	}

	public Booking(int bookingId, Date bookingDate, int noOfSeatsBooked, Double amount, Bus bus, User user) {
		super();
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.noOfSeatsBooked = noOfSeatsBooked;
		this.amount = amount;
		this.bus = bus;
		this.user = user;
	}

	public Booking(int bookingId, Date bookingDate, int noOfSeatsBooked, Double amount, Bus bus) {
		super();
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.noOfSeatsBooked = noOfSeatsBooked;
		this.amount = amount;
		this.bus = bus;
	}

	public Booking(int bookingId, Date bookingDate, int noOfSeatsBooked, Double amount, User user) {
		super();
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.noOfSeatsBooked = noOfSeatsBooked;
		this.amount = amount;
		this.user = user;
	}

	public int getBookingId() {
		return bookingId;
	}

	public void setBookingId(int bookingId) {
		this.bookingId = bookingId;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public int getNoOfSeatsBooked() {
		return noOfSeatsBooked;
	}

	public void setNoOfSeatsBooked(int noOfSeatsBooked) {
		this.noOfSeatsBooked = noOfSeatsBooked;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Bus getBus() {
		return bus;
	}

	public void setBus(Bus bus) {
		this.bus = bus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Booking [bookingId=" + bookingId + ", bookingDate=" + bookingDate + ", noOfSeatsBooked="
				+ noOfSeatsBooked + ", amount=" + amount + ", bus=" + bus + ", user=" + user + "]";
	}

	
	
}
