package com.bus.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bus.dto.BookingDto;
import com.bus.model.Booking;
import com.bus.service.BusBookingService;

@RestController
public class BusBookingController {

	@Autowired
	BusBookingService busBookingService;
	
	
	@PostMapping("booking")
	public ResponseEntity<Booking> makeBooking(@RequestBody BookingDto bookingDto) {
		
	Booking booking=busBookingService.makeBooking(bookingDto);
		 
		 return new ResponseEntity<Booking>(booking,HttpStatus.OK);
		
	}
	
	
	
	@PostMapping("/bookings/{date}")
	public ResponseEntity<List<Booking>> makeBooking(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
		
	List<Booking> booking=busBookingService.getBooking(date);
		 
		 return new ResponseEntity<List<Booking>>(booking,HttpStatus.OK);
		
	}

}
