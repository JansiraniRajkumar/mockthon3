package com.bus.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bus.dto.BusDto;
import com.bus.model.Bus;
import com.bus.service.BusService;

@RestController
public class BusController {

	@Autowired
	BusService busService;

	@PostMapping(value="/bus")
	public Bus createBus(@RequestBody Bus bus) {
		Bus buses = busService.createBus(bus);
		return buses;
	}

	@PostMapping(value="/buses")
	public ResponseEntity<List<Bus>> searchbus(@RequestBody BusDto busDto) {
		List<Bus> Buses = busService.searchbus(busDto);	
		return new ResponseEntity<List<Bus>>(Buses,HttpStatus.OK);
	
	}
	}
