package com.bus.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.bus.dto.ConverterService;
import com.bus.dto.UserDTO;
import com.bus.model.User;
import com.bus.service.BookListService;


@RestController
public class BookingList {

	@Autowired
	private BookListService userService;

	@Autowired
	private ConverterService converterService;

	@GetMapping(value = "/user/{id}")
	@ResponseStatus(HttpStatus.OK)
	public UserDTO getUserById(@PathVariable("id") int id) {

		User user = userService.getUserById(id);
		return converterService.convertToDto(user);
	}

}
