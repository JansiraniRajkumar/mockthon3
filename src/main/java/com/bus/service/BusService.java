package com.bus.service;

import java.util.List;

import com.bus.dto.BusDto;
import com.bus.model.Bus;

public interface BusService {

	public Bus createBus(Bus bus);

	public List<Bus> searchbus(BusDto busDto);

}
