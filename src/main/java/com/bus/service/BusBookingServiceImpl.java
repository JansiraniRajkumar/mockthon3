package com.bus.service;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bus.dto.BookingDto;
import com.bus.exception.DataNotFound;
import com.bus.exception.UserNotfoundException;
import com.bus.model.Booking;
import com.bus.model.Bus;
import com.bus.model.User;
import com.bus.repository.BusBookingRepo;
import com.bus.repository.BusRepo;
import com.bus.repository.UserRepo;
@Service
public class BusBookingServiceImpl implements BusBookingService {
	
	@Autowired
	BusBookingRepo busBookingRepo;
	
	@Autowired
	BusRepo busRepo;
	@Autowired
	UserRepo userRepo;

	@Override
	public Booking makeBooking(BookingDto bookingDto) {
		Booking booking =new Booking();
		BeanUtils.copyProperties(bookingDto, booking);
	
	User user	=userRepo.findByEmail(bookingDto.getEmailId());
	if(user!=null) {
		
		Bus bus =busRepo.findById(bookingDto.getBusId()).orElseThrow(()-> new DataNotFound());
		
		bus.setNoOfSeatsAvailable(bus.getNoOfSeatsAvailable()-bookingDto.getNoOfSeatsBooked());
		busRepo.save(bus);
		booking.setAmount(bus.getFare()*bookingDto.getNoOfSeatsBooked());
		booking.setUser(user);
		booking.setBus(bus);
		booking.setJourneyDate(bus.getJourneyDate());
		
		return busBookingRepo.save(booking);
	}
	else {
		throw new UserNotfoundException("requested user is not there");
	}
			
		
	}

	@Override
	public List<Booking> getBooking(Date date) {
		
		return busBookingRepo.findByJourneyDate(date);
	}

}
