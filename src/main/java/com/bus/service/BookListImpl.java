package com.bus.service;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.bus.exception.UserNotfoundException;
import com.bus.model.User;
import com.bus.repository.UserRepo;


@Service
@Transactional
public class BookListImpl implements BookListService {

	@Autowired
	private UserRepo userRepository;

	@Override
	public User getUserById(int userId) {
		Optional<User> option = userRepository.findById(userId);
		User user = null;
		if (option.isPresent()) {
			user = option.get();

		} else {
			throw new UserNotfoundException("User with id: " + userId + "  Not found");
		}
		return user;
	}

}
