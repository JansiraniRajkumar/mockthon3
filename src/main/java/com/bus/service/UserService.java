package com.bus.service;

import java.util.List;

import com.bus.dto.UserLoginDto;
import com.bus.dto.UserRegistrationDto;
import com.bus.model.User;

public interface UserService {

	List<User> getAllUsers();

	UserLoginDto createUser(UserRegistrationDto userRegistrationDto);

}
