package com.bus.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bus.dto.BusDto;
import com.bus.exception.BusNotFoundException;
import com.bus.model.Bus;
import com.bus.repository.BusRepo;


@Service
public class BusServiceImpl implements BusService {

	@Autowired
	BusRepo busRepository;

	@Override
	public Bus createBus(Bus bus) {
		return busRepository.save(bus);
	}

	@Override
	public List<Bus> searchbus(BusDto busDto) throws BusNotFoundException {
		
		
		
		List<Bus> buses = busRepository.findBusBySourceAndDestinationAndJourneyDate(busDto.getSource(),busDto.getDestination(),busDto.getJourneyDate());
		if(buses.isEmpty()) {
			throw new BusNotFoundException("bus is not available");
		}
		return buses;
	}

}
